

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import zomato.db;

/**
 * Servlet implementation class savefile
 */
@WebServlet("/savefile")
public class savefile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public savefile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		String pathheader=request.getParameter("file");
		BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(pathheader));
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] words = line.split(",");
					String url=words[0]; 
					String name=(words[1]);
					String address=(words[2]);
					String location=(words[3]);
					String cuisine=(words[4]);
					String dishes=(words[5]);
					String price=(words[6]);
					String ratings=(words[7]);
					String votes=(words[8]);
					try{  
						Connection con =db.initializeDatabase();
						//Class.forName("com.mysql.jdbc.Driver");  
						//Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3307/zomato","root","password");  
						  
						PreparedStatement ps=con.prepareStatement(  
						"insert into details values(?,?,?,?,?,?,?,?,?)");  
						  
						ps.setString(1,url); 
						ps.setString(2,name); 
						ps.setString(3,address); 
						ps.setString(4,location); 
						ps.setString(5,cuisine); 
						ps.setString(6,dishes); 
						ps.setString(7,price); 
						ps.setString(8,ratings); 
						ps.setString(9,votes); 
						          
						int i=ps.executeUpdate();  
						if(i>0)  
							request.getRequestDispatcher("confirm.jsp").forward(request, response);
						  else
				                request.getRequestDispatcher("error.jsp").forward(request, response);
						      
						          
						}
					catch (Exception e2) 
					{
						System.out.println(e2);
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		doGet(request, response);
	}

}
